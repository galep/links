FROM nginx:1.21.6

COPY ./src/ /usr/share/nginx/html

EXPOSE 80

HEALTHCHECK CMD curl --fail -o /dev/null http://localhost || exit 1